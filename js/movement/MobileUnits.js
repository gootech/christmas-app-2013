define([
    'goo/entities/EntityUtils',
    'goo/shapes/ShapeCreator',
    'goo/renderer/Material',
    'goo/renderer/shaders/ShaderLib',
    'js/physics/PhysicalWorld'

], function (
    EntityUtils,
    ShapeCreator,
    Material,
    ShaderLib,
    PhysicalWorld
    ) {
    'use strict';

    function sphericalMobile(world, radius, pos, velocity, mass, visualise) {
        var sphereEntity;
        if (visualise) {
            var sphereMesh = ShapeCreator.createSphere(16, 16, radius);
            var material = Material.createMaterial(ShaderLib.simpleLit);
            sphereEntity = EntityUtils.createTypicalEntity(world, sphereMesh, material);
        } else {
            sphereEntity = EntityUtils.createTypicalEntity(world);
        }

        sphereEntity.ammoComponent = PhysicalWorld.createAmmoJSSphere(radius, pos, velocity, mass);
        sphereEntity.setComponent(PhysicalWorld.createAmmoComponentScript());
        sphereEntity.addToWorld();
        return sphereEntity;
    }

    return {
        sphericalMobile:sphericalMobile
    }

});
