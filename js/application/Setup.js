require([
    'js/application/GooInitiator',
    'js/game/GameData'

], function (
    GooInitiator,
    GameData
    ) {
    'use strict';

    function dataLoaded() {
        GooInitiator.buildPhysicalWorld(GameData.LEVEL_DATA);
        GooInitiator.configureMobilePieces(GameData.MOBILE_PIECES);
        GooInitiator.setupComplete();
    }

    function init() {
        GooInitiator.initGoo();
        GooInitiator.loadGameData(GameData, dataLoaded);
    }

    init();
});
