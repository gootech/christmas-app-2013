define({
    LEVEL_DATA:{
        bundles:['snowworld.bundle'],
        physicalMeshes:['snowworld/entities/snowworld_0.entity', 'snowworld/entities/snowworld_1.entity'],
        spawnPoints:[[12,0,2], [-2,0,-11]]
    },
    MOBILE_PIECES:{
        bundles:[/*'zombie.bundle',*/ 'zombiewalk.bundle'],
        entities:[
        //    {ref:'walking/entities/RootNode.entity', scale:[0.018, 0.018, 0.018], physicalRadius: 0.7, health:50},
            {ref:'zombiewalk/entities/RootNode.entity', scale:[0.28, 0.28, 0.28], physicalRadius: 1.7, health:50}
        ]
    }
});