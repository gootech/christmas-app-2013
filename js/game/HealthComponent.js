define([
    'goo/entities/components/Component',
    'js/application/Game'
], function(
    Component,
    Game
    ){

    function HealthComponent(ent, n0){
        this.type = "HealthComponent";
        this.entity = ent;
        this.maxHealth = this.currentHealth = n0;
        Game.register(ent.id+"_TakeDamage", this, applyDamage);
    }
    HealthComponent.prototype = Object.create(Component.prototype);
    function applyDamage(n0){
        this.currentHealth -= n0;
        console.log("Removing:"+n0+" health.");
        if(this.currentHealth <= 0){
            this.currentHealth = 0;
            Game.unregister(this.entity.id+"_TakeDamage", this);
            Game.raiseEvent(this.entity.id+"_Dead", this.entity);
            //this.entity.aIComponent.getBehaviorByName("Death").active = true;
        }
        console.log(this.currentHealth+"/"+this.maxHealth+" left.");
    }

    return HealthComponent;
});