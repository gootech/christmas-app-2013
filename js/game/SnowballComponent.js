define([
    'goo/entities/components/Component',
    'js/application/Game',
    'goo/entities/EntityUtils',
    'goo/renderer/shaders/ShaderLib',
    'goo/renderer/TextureCreator',
    'goo/shapes/ShapeCreator',
    'goo/renderer/Material',
    'goo/addons/howler/systems/HowlerSystem',
    'goo/addons/howler/components/HowlerComponent',
    'goo/math/Ray',
    'goo/math/Vector3',
    'js/effects/Snowball',
    'js/movement/MobileUnits'

], function(
    Component,
    Game,
    EntityUtils,
    ShaderLib,
    TextureCreator,
    ShapeCreator,
    Material,
    HowlerSystem,
    HowlerComponent,
    Ray,
    Vector3,
    Snowball,
    MobileUnits
    ) {
    'use strict';
    var tosser;
    var sound;
    var calcVec = new Vector3();
    function SnowballComponent(){
        this.type = "SnowballComponent";
        sound = new Howl({urls: ["res/sounds/swish.ogg", "res/sounds/swish.mp3"], volume:0.3});

        var mesh = ShapeCreator.createSphere(16, 16, 0.3);
        var mat = Material.createMaterial( ShaderLib.simpleLit, 'BoxMaterial');
        var tossball = EntityUtils.createTypicalEntity( Game.goo.world, mesh, mat);
        tossball.addToWorld();
        tossball.transformComponent.setTranslation( 0, 0.1, 0.1);

        tosser = EntityUtils.createTypicalEntity( Game.goo.world);
        tosser.addToWorld();

        tosser.transformComponent.attachChild( tossball.transformComponent);

        tosser.transformComponent.setTranslation( 0, 0, 0 );

        tosser.transformComponent.setRotation( 0.15, 0.1, 4); // rotate the shotty a bit.
        Game.viewCam.transformComponent.attachChild(tosser.transformComponent);
        Game.viewCam.transformComponent.setUpdated();
        Game.register("MouseButton1", this, mouseButton1);
    }
    SnowballComponent.prototype = Object.create(Component.prototype);

    function resetSSG() {
        tosser.transformComponent.setRotation( 0.15, 0.1, 0);
    }

    var snowball;

    var pickingStore = {};
    var md_pos = new Vector3();
    var md_dir = new Vector3();
    var md_ray = new Ray();
    function toss( camera, x, y, w, h) {

        snowball = snowball || new Snowball(Game.goo);

        Game.goo.renderer.pick( x, y, pickingStore, camera);
        if( pickingStore.id == -1)
            return;
        camera.getPickRay( x, y, w, h, md_ray);

        var pos = Game.userEntity.transformComponent.transform.translation.data;
        calcVec.set(md_ray.direction);
        var size = 0.2+Math.random()*0.3
        var mass = 10+Math.random()*50
        calcVec.mul(10 +(15000 / (mass*mass)));

        MobileUnits.sphericalMobile(Game.goo.world, size, [pos[0], pos[1]+1.3, pos[2]], calcVec.data, mass, true);


        md_ray.direction.mul( pickingStore.depth);
        md_ray.origin.add( md_ray.direction);

        var entity = Game.goo.world.entityManager.getEntityById(pickingStore.id);

        if( ! entity.transformComponent.parent )
            return;

        var p = entity.transformComponent.parent.entity;
        if( ! p.animationComponent )
            return;
        //var eac = p.animationComponent;

        snowball.spawn(md_ray.origin);
        console.log("Shooting Entity:");
        console.log(p);
        Game.raiseEvent(p.id+"_TakeDamage", 7);
        //p.healthComponent.applyDamage(7);
    }

    function randInt(max) {
        return Math.floor(Math.random()*max);
    }
    function randBlood() {
        return randInt(200)-100;
    }

    function mouseButton1(bool0){
        //console.log(bool0);
        if(true == bool0){
            //console.log("bam");
            //	if(document.pointerLockElement) {
            //console.log( Game.userEntity.transformComponent.transform.translation.data );
            sound.play();
            tosser.transformComponent.setRotation( 0.35, 0.1, 0);
            setTimeout( resetSSG, 250);


            var w = Game.goo.renderer.viewportWidth;
            var h = Game.goo.renderer.viewportHeight;
            var x = w / 2;
            var y = h / 2;
            Game.goo.pick( x, y, function( id, depth){
                if( id < 0){return;}
                var camera = Game.viewCam.cameraComponent.camera
                toss( camera, x, y, w, h);
            });
            //	}
        }
    }
    return SnowballComponent;
});