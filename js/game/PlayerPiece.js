define([
    'js/camera/FPSCamComponent',
    'js/game/SnowballComponent',
    'js/effects/FlashlightComponent',
    'js/application/Game',
    'js/movement/MobileUnits'

], function (
    FPSCamComponent,
    SnowballComponent,
    FlashlightComponent,
    Game,
    MobileUnits
    ) {
    'use strict';

    function addPlayerPiece(world) {
        var userSphere = MobileUnits.sphericalMobile(world, 0.7, [0,0,0],[0,0,0],50, true);
        userSphere.name = "UserSphere";
        var fpsCamComponent = new FPSCamComponent(userSphere);
        fpsCamComponent.setHeight(1.7);
        Game.userEntity = fpsCamComponent.moveEntity;
        Game.userEntity.setComponent(fpsCamComponent);
        Game.snowball = new SnowballComponent();
        Game.userEntity.setComponent(Game.snowball);
        Game.userEntity.setComponent(new FlashlightComponent());
    }

    return {
        addPlayerPiece:addPlayerPiece
    }

});
