define([
    'goo/entities/EntityUtils',
    'goo/entities/components/ScriptComponent',
    'goo/shapes/ShapeCreator',
    'goo/renderer/Material',
    'goo/renderer/shaders/ShaderLib',
    'js/movement/SphereSpatial',
    'js/application/Game',
    'js/game/HealthComponent',
    'js/physics/PhysicalWorld',
    'js/movement/MobileUnits'

], function (
    EntityUtils,
    ScriptComponent,
    ShapeCreator,
    Material,
    ShaderLib,
    SphereSpatial,
    Game,
    HealthComponent,
    PhysicalWorld,
    MobileUnits
    ) {
    'use strict';

    var world;
    var levelData;
    var mobs = {};

    function registerMobile(ref, mobEntity, mobData) {
        mobs[ref] = {entity:mobEntity, mobData:mobData};
    }

    function setLevelData(data) {
        console.log("Level Data:", data);
        levelData = data;
    }

    function setWorld(gooWorld) {
        world = gooWorld;
    }

    function createMobEntity(world, mob) {
        var e = EntityUtils.clone(world, mob.entity);
        e.setComponent(new HealthComponent(e, mob.mobData.health));
        Game.register(e.id+"_Dead", e, destroyMob);
        e.addToWorld();
        return e;
    }

    function addEntityToMobile(sphereEntity, world, mob) {
        var mobile = createMobEntity(world, mob);
        mobile.transformComponent.transform.translation.sub_d(0,  mob.mobData.physicalRadius, 0);
        var moveParent = EntityUtils.createTypicalEntity(world);
        moveParent.sphereEntity = sphereEntity;
        PhysicalWorld.attachSphericalMovementScript(moveParent, mobile);
        moveParent.transformComponent.attachChild(mobile.transformComponent);

        moveParent.addToWorld();
    }

    function getRandomSpawnPoint() {
        var selection = Math.floor(Math.random()*levelData.spawnPoints.length);
        return levelData.spawnPoints[selection];
    }

    function spawnMobile(mobRef) {
        // The sphere handles physical movement

        var pos = getRandomSpawnPoint();

        var movableSphere = MobileUnits.sphericalMobile(world, mobs[mobRef].mobData.physicalRadius, pos,[0,0,0],10, false);
        addEntityToMobile(movableSphere, world, mobs[mobRef]);

        var spatialControl = new SphereSpatial(movableSphere);
        movableSphere.spatialControl = spatialControl;
        spatialControl.walk();
    }

    function destroyMob(){
        console.log("Destroy mob and cleaning up");
        var p = this.transformComponent.parent.entity;
        PhysicalWorld.removeAmmoComponent(p.sphereEntity.ammoComponent);
        p.sphereEntity.spatialControl.remove();
        delete p.sphereEntity.spatialControl;
        p.sphereEntity.removeFromWorld();
        p.removeFromWorld();
    }

    function startAutoSpawn() {
        spawnMobile('zombiewalk/entities/RootNode.entity');
        spawnMobile('zombiewalk/entities/RootNode.entity');
        setInterval(function() {
            spawnMobile('zombiewalk/entities/RootNode.entity');
        },3000)
    }

    return {
        setWorld:setWorld,
        setLevelData:setLevelData,
        registerMobile:registerMobile,
        spawnMobile:spawnMobile,
        startAutoSpawn:startAutoSpawn
    }
});
