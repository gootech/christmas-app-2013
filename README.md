Christmas Special example project
=================================

This example intends to show the basics of how to use assets created with
the Goo Create tool in a context where you write your own code. Feel welcome
to use this code for building an app on the Goo Engine.

###Basic workflow

- Pull this project.
- Serve the index.html from a web server of choice. (Needed for loading data.)
- Open the served index.html in your browser.
- Use WASD keys to walk around.
- Jump and spawn enemies with space bar.
- "Toss" snowball by pressing the mouse button.
- Take a look at the code.

The snowball is not really a snowball, it is made from two separate things. One is a particle effect which plays if a
ray strikes a target. The other is a physics sphere which is also rendered as a visual sphere.

###Asset Pipeline

- Make a new asset using Goo Create.
- Export the asset as a bundle.

You should now have a zip file on your computer containing the asset data. Add
the contents of this zip to the res folder. It is recommended to rename the
root.bundle file to something memorable, perhaps my_new_thing.bundle fits the bill here.

Now open the GameData.js file located in the js/game folder. Add the 'my_new_thing.bundle' to the array of bundles
in LEVEL_DATA. Reload index.html and see if it works. You should be able to see the new
asset.

###A piece of advice:
Run things through this pipeline in reasonably small and quick spurts.
There are quirks you will find which sometimes cause your edits to cause problems. By
doing many small updates rather than a few big ones you will reduce the overall risk of
disasters in your project.

##A closer look at the code

The game code starts with Setup.js which uses GooInitiator to run an initiation sequence.
This gets the engine running and loads your assets listed in the GameData object. Note that
you are probably going to expand on this concept if your project grows a bit. This method of
setting up the project is structured for educational purposes and you should change when needed.

From this point you are likely to be ready enough for coding your own app either by expanding on
this project or starting your own from scratch.

###Physics

We are using a JS port of the bullet physics library. With this we get the ability to walk
around on visual shapes which is quite cool and useful. This physics engine also supports a whole
lot of additional features, however we at goo have only been playing around with this for about a
week so we don't have much experience or recommendations for how to work with the system. Take a
look at PhysicalWorld to see how we use this.

The physics implementation in place here does not treat transforms the same way as the visuals. The
current code expects your physical trimesh meshes to be untransformed. If you desire to transform
these shapes you will need to add some code to handle that.

###Mobile Devices

For mobile devices we recommend that you use the hammer.js lib which is included here. If you decide
to go for the mobile device approach you will face a couple of special challenges regarding input
mechanics and device performance. Since these span a wide possibility space it is up to you to
decide how to best handle the details.